<div class="product product-single row mb-7">
    <div class="col-md-6 sticky-sidebar-wrapper">
        <div class="product-gallery pg-vertical sticky-sidebar"
            data-sticky-options="{'minWidth': 767}">
            <div
                class="product-single-carousel owl-carousel owl-theme owl-nav-inner row cols-1 gutter-no">
                <figure class="product-image">
                    <img src="{{ asset('ui/frontend/images/product/product-1-1-580x652.jpg') }}"
                        data-zoom-image="images/product/product-1-1-800x900.jpg"
                        alt="Women's Brown Leather Backpacks" width="800" height="900">
                </figure>
                <figure class="product-image">
                    <img src="{{ asset('ui/frontend/images/product/product-1-2-580x652.jpg ')}}"
                        data-zoom-image="images/product/product-1-2-800x900.jpg"
                        alt="Women's Brown Leather Backpacks" width="800" height="900">
                </figure>
                <figure class="product-image">
                    <img src="{{ asset('ui/frontend/images/product/product-1-3-580x652.jpg')}}"
                        data-zoom-image="images/product/product-1-3-800x900.jpg"
                        alt="Women's Brown Leather Backpacks" width="800" height="900">
                </figure>
                <figure class="product-image">
                    <img src="{{ asset('ui/frontend/images/product/product-1-4-580x652.jpg')}}"
                        data-zoom-image="images/product/product-1-4-800x900.jpg"
                        alt="Women's Brown Leather Backpacks" width="800" height="900">
                </figure>
            </div>
            <div class="product-thumbs-wrap">
                <div class="product-thumbs">
                    <div class="product-thumb active">
                        <img src="{{ asset('ui/frontend/images/product/product-1-1-109x122.jpg')}}" alt="product thumbnail"
                            width="109" height="122">
                    </div>
                    <div class="product-thumb">
                        <img src="{{ asset('ui/frontend/images/product/product-1-2-109x122.jpg')}}" alt="product thumbnail"
                            width="109" height="122">
                    </div>
                    <div class="product-thumb">
                        <img src="{{ asset('ui/frontend/images/product/product-1-3-109x122.jpg')}}" alt="product thumbnail"
                            width="109" height="122">
                    </div>
                    <div class="product-thumb">
                        <img src="{{ asset('ui/frontend/images/product/product-1-4-109x122.jpg')}}" alt="product thumbnail"
                            width="109" height="122">
                    </div>
                </div>
                <button class="thumb-up disabled"><i class="fas fa-chevron-left"></i></button>
                <button class="thumb-down disabled"><i class="fas fa-chevron-right"></i></button>
            </div>
            <div class="product-label-group">
                <label class="product-label label-new">new</label>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="product-details">
            <div class="product-navigation">
                <ul class="breadcrumb breadcrumb-lg">
                    <li><a href="demo1.html"><i class="d-icon-home"></i></a></li>
                    <li><a href="#" class="active">Products</a></li>
                    <li>Detail</li>
                </ul>

                <ul class="product-nav">
                    <li class="product-nav-prev">
                        <a href="#">
                            <i class="d-icon-arrow-left"></i> Prev
                            <span class="product-nav-popup">
                                <img src="images/product/product-thumb-prev.jpg"
                                    alt="product thumbnail" width="110" height="123">
                                <span class="product-name">Sed egtas Dnte Comfort</span>
                            </span>
                        </a>
                    </li>
                    <li class="product-nav-next">
                        <a href="#">
                            Next <i class="d-icon-arrow-right"></i>
                            <span class="product-nav-popup">
                                <img src="images/product/product-thumb-next.jpg"
                                    alt="product thumbnail" width="110" height="123">
                                <span class="product-name">Sed egtas Dnte Comfort</span>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>

            <h1 class="product-name">Converse Training Shoes</h1>
            <div class="product-meta">
                SKU: <span class="product-sku">12345670</span>
                BRAND: <span class="product-brand">The Northland</span>
            </div>
            <div class="product-price">$113.00</div>
            <div class="ratings-container">
                <div class="ratings-full">
                    <span class="ratings" style="width:80%"></span>
                    <span class="tooltiptext tooltip-top"></span>
                </div>
                <a href="#product-tab-reviews" class="link-to-tab rating-reviews">( 11 reviews )</a>
            </div>
            <p class="product-short-desc">Sed egestas, ante et vulputate volutpat, eros pede semper
                est, vitae luctus metus libero eu augue. Morbi purus liberpuro ate vol faucibus
                adipiscing.</p>
            <div class="product-form product-variations product-color">
                <label>Color:</label>
                <div class="select-box">
                    <select name="color" class="form-control">
                        <option value="" selected="selected">Choose an Option</option>
                        <option value="white">White</option>
                        <option value="black">Black</option>
                        <option value="brown">Brown</option>
                        <option value="red">Red</option>
                        <option value="green">Green</option>
                        <option value="yellow">Yellow</option>
                    </select>
                </div>
            </div>
            <div class="product-form product-variations product-size">
                <label>Size:</label>
                <div class="product-form-group">
                    <div class="select-box">
                        <select name="size" class="form-control">
                            <option value="" selected="selected">Choose an Option</option>
                            <option value="s">Small</option>
                            <option value="m">Medium</option>
                            <option value="l">Large</option>
                            <option value="xl">Extra Large</option>
                        </select>
                    </div>
                    <a href="#" class="product-variation-clean" style="display: none;">Clean All</a>
                </div>
            </div>
            <div class="product-variation-price">
                <span>$239.00</span>
            </div>

            <hr class="product-divider">

            <div class="product-form product-qty">
                <div class="product-form-group">
                    <div class="input-group mr-2">
                        <button class="quantity-minus d-icon-minus"></button>
                        <input class="quantity form-control" type="number" min="1" max="1000000">
                        <button class="quantity-plus d-icon-plus"></button>
                    </div>
                    <button
                        class="btn-product btn-cart text-normal ls-normal font-weight-semi-bold"><i
                            class="d-icon-bag"></i>Add to
                        Cart</button>
                </div>
            </div>

            <hr class="product-divider mb-3">

            <div class="product-footer">
                <div class="social-links mr-4">
                    <a href="#" class="social-link social-facebook fab fa-facebook-f"></a>
                    <a href="#" class="social-link social-twitter fab fa-twitter"></a>
                    <a href="#" class="social-link social-pinterest fab fa-pinterest-p"></a>
                </div>
                <span class="divider d-lg-show"></span>
                <a href="#" class="btn-product btn-wishlist mr-6"><i class="d-icon-heart"></i>Add to
                    wishlist</a>
                <a href="#" class="btn-product btn-compare"><i class="d-icon-compare"></i>Add
                    to
                    compare</a>
            </div>
        </div>
    </div>
</div>