@props(['name', 'options', 'selected' => ''])

<x-frontend.form.field>
  
    <select name="{{ $name }}" id="{{ $name }}" class="form-select">
        @foreach ($options as $key=>$value)
        <option value="{{ $key }}" {{ $key == $selected ? 'selected' : '' }}>{{ $value }}</option>
        @endforeach
    </select>

</x-frontend.form.field>