@props(['name'])

<x-frontend.form.field>
    <textarea name="{{ $name }}" class="form-control" id="{{ $name }}">

    {{ $slot ?? old($name) }}

    </textarea>
    
    <x-frontend.form.label name="{{ $name }}" />

    <x-frontend.form.error name="{{ $name }}" />
    
</x-frontend.form.field>