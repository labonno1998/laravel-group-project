@props(['name'])

<x-frontend.form.field>

    <input name="{{ $name }}" class="form-control" id="{{ $name }}" {{ $attributes }}>

    <x-frontend.form.label name="{{ $name }}" />

    <x-frontend.form.error name="{{ $name }}" />

</x-frontend.form.field>