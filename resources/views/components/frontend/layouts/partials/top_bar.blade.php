<div class="page-wrapper">
    <h1 class="d-none">Riode - Responsive eCommerce HTML Template</h1>
    <header class="header">
        <div class="header-middle sticky-header fix-top sticky-content has-center">
            <div class="container">
                <div class="header-left">
                    <a href="#" class="mobile-menu-toggle">
                        <i class="d-icon-bars2"></i>
                    </a>
                    <nav class="main-nav">
                        <ul class="menu menu-active-underline">
                            <li class="active">
                                <a href="{{ route('frontend.home') }}">Home</a>
                            </li>
                           
                            <!-- End of Dropdown -->
                            <li>
                                <a href="{{ route('frontend.product') }}">Product</a>
                                <div class="megamenu">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <h4 class="menu-title">Elements 1</h4>
                                            <ul>
                                                <li><a href="element-accordions.html">Accordions</a></li>
                                                <li><a href="element-alerts.html">Alert &amp; Notification</a></li>

                                                <li><a href="element-banner-effect.html">Banner Effect

                                                    </a></li>
                                                <li><a href="element-banner.html">Banner
                                                    </a></li>
                                                <li><a href="element-blog-posts.html">Blog Posts</a></li> 
                                                <li><a href="element-buttons.html">Buttons</a></li>
                                                <li><a href="element-cta.html">Call to Action</a></li>
                                                <li><a href="element-countdown.html">Countdown
                                                    </a></li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </li>
                            <!-- End of Dropdown -->
                            <li>
                                <a href="#">Order</a>
                                <ul>
                                    <li><a href="about-us.html">About</a></li>
                                    <li><a href="contact-us.html">Contact Us</a></li>
                                    <li><a href="account.html">My Account</a></li>
                                    <li><a href="wishlist.html">Wishlist</a></li>
                                    <li><a href="faq.html">FAQs</a></li>
                                    <li><a href="error-404.html">Error 404</a>
                                    <li><a href="coming-soon.html">Coming Soon</a></li>
                                </ul>
                            </li>
                            <!-- End of Dropdown -->

                            <li >
                                <a href="{{ route('frontend.cart') }}">cart</a>
                            </li>
                            
                             <!-- End of Dropdown -->

                            <li >
                                <a href="{{ route('frontend.store') }}">store</a>
                            </li>

                             <!-- End of Dropdown -->

                            <li>
                                <a href="{{ route('frontend.contact') }}">About us</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="header-center">
                    <a href="demo6.html" class="logo">
                        <img src="{{ asset('ui/frontend/images/demos/demo6/logo.png') }}" alt="logo" width="153" height="44" />
                    </a>
                    <!-- End of Logo -->

                </div>
                <div class="dropdown cart-dropdown off-canvas">
                    <a href="#" class="cart-toggle">
                        <span class="cart-label d-lg-show">
                            <span class="cart-name">My cart</span>
                            <span class="cart-name-delimiter">/</span>
                            <span class="cart-price">$0.00</span>
                        </span>
                        <span class="cart-count">(2<span> Items</span>)</span>
                    </a>
                    <!-- End of Cart Toggle -->
                    <div class="canvas-overlay"></div>
                    <!-- End Cart Toggle -->
                    <div class="dropdown-box">
                        <div class="canvas-header">
                            <h4 class="canvas-title">Shopping Cart</h4>
                            <a href="#" class="btn btn-dark btn-link btn-icon-right btn-close">close<i
                                    class="d-icon-arrow-right"></i><span class="sr-only">Cart</span></a>
                        </div>
                        <div class="products scrollable">
                            <div class="product product-cart">
                                <figure class="product-media">
                                    <a href="#">
                                        <img src="{{ asset('ui/frontend/images/cart/product-1.jpg') }}" alt="product" width="80"
                                            height="88" />
                                    </a>
                                    <button class="btn btn-link btn-close">
                                        <i class="fas fa-times"></i><span class="sr-only">Close</span>
                                    </button>
                                </figure>
                                <div class="product-detail">
                                    <a href="demo5-product.html" class="product-name">Riode White Trends</a>
                                    <div class="price-box">
                                        <span class="product-quantity">1</span>
                                        <span class="product-price">$21.00</span>
                                    </div>
                                </div>

                            </div>
                            <!-- End of Cart Product -->
                            <div class="product product-cart">
                                <figure class="product-media">
                                    <a href="#">
                                        <img src="{{ asset('ui/frontend/images/cart/product-2.jpg') }}" alt="product" width="80"
                                            height="88" />
                                    </a>
                                    <button class="btn btn-link btn-close">
                                        <i class="fas fa-times"></i><span class="sr-only">Close</span>
                                    </button>
                                </figure>
                                <div class="product-detail">
                                    <a href="demo5-product.html" class="product-name">Dark Blue Women’s Leomora
                                        Hat</a>
                                    <div class="price-box">
                                        <span class="product-quantity">1</span>
                                        <span class="product-price">$118.00</span>
                                    </div>
                                </div>
                            </div>
                            <!-- End of Cart Product -->
                        </div>
                        <!-- End of Products  -->
                        <div class="cart-total">
                            <label>Subtotal:</label>
                            <span class="price">$139.00</span>
                        </div>
                        <!-- End of Cart Total -->
                        <div class="cart-action">
                            <a href="{{ route('frontend.cart') }}" class="btn btn-dark btn-link">View Cart</a>
                            <a href="{{ route('frontend.checkout') }}" class="btn btn-dark"><span>Go To Checkout</span></a>
                        </div>
                        <!-- End of Cart Action -->
                    </div>
                    <!-- End Dropdown Box -->
                </div>

                <div class="header-search hs-toggle mobile-search">
                    <a href="#" class="search-toggle">
                        <i class="d-icon-search"></i>
                    </a>
                    <form action="#" class="input-wrapper">
                        <input type="text" class="form-control" name="search" autocomplete="off"
                            placeholder="Search your keyword..." required />
                        <button class="btn btn-search" type="submit" title="submit-button">
                            <i class="d-icon-search"></i>
                        </button>
                    </form>
                </div>
                <!-- End of Header Search -->
            </div>
        </div>

    </div>
</header>
<!-- End of Header -->