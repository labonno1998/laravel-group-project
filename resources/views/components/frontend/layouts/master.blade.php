<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
		 
		<title>{{ $pageTitle ?? 'Laravel E-Commerce' }}</title>
	 
		 <!-- Favicon -->
		 <link rel="icon" type="image/png" href="{{ asset('ui/frontend/images/icons/favicon.png') }}">
		 <!-- Preload Font -->
		 <link rel="preload" href="{{ asset('ui/frontend/fonts/riode.ttf?5gap68') }}" as="font" type="font/woff2" crossorigin="anonymous">
		 <link rel="preload" href="{{ asset('ui/frontend/vendor/fontawesome-free/webfonts/fa-solid-900.woff2') }}" as="font" type="font/woff2"
			 crossorigin="anonymous">
		 <link rel="preload" href="{{ asset('ui/frontend/vendor/fontawesome-free/webfonts/fa-brands-400.woff2') }}" as="font" type="font/woff2"
			 crossorigin="anonymous">
		 <script>
			 WebFontConfig = {
				 google: { families: [ 'Jost:400,500,600,700,800,900' ] }
			 };
			 ( function ( d ) {
				 var wf = d.createElement( 'script' ), s = d.scripts[ 0 ];
				 wf.src = 'js/webfont.js';
				 wf.async = true;
				 s.parentNode.insertBefore( wf, s );
			 } )( document );
		 </script>
	 
	 
		 <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend/vendor/fontawesome-free/css/all.min.css') }}">
		 <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend/vendor/animate/animate.min.css') }}">
	 
		 <!-- Plugins CSS File -->
		 <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend/vendor/magnific-popup/magnific-popup.min.css') }}">
		 <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend/vendor/owl-carousel/owl.carousel.min.css') }}">
		 <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend/vendor/nouislider/nouislider.min.css') }}">
		 <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend/vendor/sticky-icon/stickyicon.css') }}">
	 
	 
		 <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend/vendor/sticky-icon/stickyicon.css') }}">
	 
		 <!-- Main CSS File -->
		 <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend/css/demo6.min.css') }}">
		 <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend/css/style.min.css') }}">



	 </head>

	<body>
		<!-- HEADER -->
		<header>
		<x-frontend.layouts.partials.top_bar/>
		</header>
		<!-- /HEADER -->

		<!-- NAVIGATION -->
		{{-- <nav id="navigation">
            <x-frontend.layouts.partials.nav />
		</nav> --}}
		<!-- /NAVIGATION -->



			<!-- container -->
				<!-- row -->
			<main>
				<div class="container">

					{{-- @yield('content') --}}
					{{ $slot }}

				</div>
				
				<!-- /row -->
			<!-- /container -->

		
		<!-- FOOTER -->
		<footer>
            <x-frontend.layouts.partials.footer />
		</footer>
			
		<!-- /FOOTER -->

		<!-- Plugins JS File -->
		<script src="{{ asset('ui/frontend/vendor/jquery/jquery.min.js') }}"></script>
		<script src="{{ asset('ui/frontend/vendor/elevatezoom/jquery.elevatezoom.min.js') }}"></script>
		<script src="{{ asset('ui/frontend/vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
		<script src="{{ asset('ui/frontend/vendor/jquery.floating/jquery.floating.min.js') }}"></script>
		<script src="{{ asset('ui/frontend/vendor/owl-carousel/owl.carousel.min.js') }}"></script>
		<script src="{{ asset('ui/frontend/vendor/imagesloaded/imagesloaded.pkgd.min.js') }}"></script>
		<script src="{{ asset('ui/frontend/vendor/isotope/isotope.pkgd.min.js') }}"></script>
		<script src="{{ asset('ui/frontend/vendor/sticky/sticky.min.js') }}"></script>
		<script src="{{ asset('ui/frontend/vendor/imagesloaded/imagesloaded.pkgd.min.js') }}"></script>
		<script src="{{ asset('ui/frontend/vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
		<script src="{{ asset('ui/frontend/vendor/isotope/isotope.pkgd.min.js') }}"></script>
		<script src="{{ asset('ui/frontend/vendor/skrollr/skrollr.min.js') }}"></script>
		<script src="{{ asset('ui/frontend/vendor/nouislider/nouislider.min.js') }}"></script>
		<!-- Main JS File -->
		<script src="{{ asset('ui/frontend/js/main.min.js') }}"></script>
		
    

	</body>
</html>
