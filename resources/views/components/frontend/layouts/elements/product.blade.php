<div class="col-md-3 col-sm-4 col-6 grid-item bags">
    <div class="product text-center">
        <figure class="product-media" style="background-color: #f9f9f9;">
            <a href="{{ route('frontend.product') }}">
                <img src="{{ asset('ui/frontend/images/demos/demo6/products/10.jpg') }}" alt="product" width="280"
                    height="315">
            </a>
            <div class="product-action-vertical">
                <a href="#" class="btn-product-icon btn-cart" data-toggle="modal"
                    data-target="#addCartModal" title="Add to cart"><i
                        class="d-icon-bag"></i></a>
                <a href="#" class="btn-product-icon btn-wishlist" title="Add to wishlist"><i
                        class="d-icon-heart"></i></a>
            </div>
            <div class="product-action">
                <a href="#" class="btn-product btn-quickview" title="Quick View">Quick
                    View</a>
            </div>
        </figure>
        <div class="product-details">
            <div class="product-cat">
                <a href="shop-grid-3cols.html">Bags & Backpack</a>
            </div>
            <h3 class="product-name">
                <a href="demo6-product.html">Best Dark blue pedestrian bag</a>
            </h3>
            <div class="product-price">
                <span class="price">$611.00</span>
            </div>
            <div class="ratings-container">
                <div class="ratings-full">
                    <span class="ratings" style="width:100%"></span>
                    <span class="tooltiptext tooltip-top"></span>
                </div>
                <a href="demo6-product.html" class="rating-reviews">( 5 reviews )</a>
            </div>
        </div>
    </div>
</div>