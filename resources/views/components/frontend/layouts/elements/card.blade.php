<div {{ $attributes->class(['product']) }}>
    @isset($image)
    <div {{ $attributes->class(['product-image']) }}>
        {{ $image }}
        <span class="product-badge">{{ $badge }}</span>
</div>
@endisset

@isset($product)

<div {{ $attributes->class(['product-details']) }}>
    @isset($category)
    <div {{ $attributes->class(['product-category']) }}>
        {{ $category }}
    </div>
    @endisset

    @isset($name)
    <div {{ $attributes->class(['product-name']) }}>
        {{ $name }}
    </div>
    @endisset

    @isset($price)
    <div {{ $attributes->class(['product-price']) }}>
        {{ $price }}
    </div>
    @endisset

    @isset($rating)
    <div {{ $attributes->class(['product-rating']) }}>
        {{ $rating }}
    </div>
    @endisset

    @isset($button)
    <div {{ $attributes->class(['product-button']) }}>
        {{ $button }}
    </div>
    @endisset

    @isset($add_to_cart)
    <div {{ $attributes->class(['product-add-to-cart']) }}>
        {{ $add_to_cart }}
    </div>
    @endisset
</div>
