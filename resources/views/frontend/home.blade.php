<x-frontend.layouts.master>
{{-- <x-components.frontend.layouts.master> --}}
    
    <x-slot name="pageTitle">
            Laravel E-commerce
    </x-slot>
              
    <main class="main">
        {{-- <div class="page-content">
            <section
                class="intro-slider owl-carousel owl-theme row owl-dot-inner animation-slider owl-nav-arrow cols-1 appear-animate"
                data-owl-options="{
                'items': 1,
                'nav': false,
                'loop': false,
                'dots': false,
                'autoplay': false,
                'responsive': {
                    '1360': {
                        'nav': true
                    }
                }
            }"> --}}
            <div class="intro-slide1 banner banner-fixed" style="background-color: #f6f6f6">
                <figure>
                    <img src="{{ asset('ui/frontend/images/demos/demo6/slides/1.png') }}" alt="slide" width="1903" height="650" />
                </figure>
                <div class="container">
                    <div class="banner-content y-50">
                        <div class="slide-animate" data-animation-options="{
                            'name': 'fadeInUpShorter',
                            'duration': '1s'
                        }">
                            <h4 class="banner-subtitle text-uppercase text-grey mb-2">Best Seller</h4>
                            <h3 class="banner-title font-weight-bold ls-m">Power bank with built in wireless
                                charge</h3>
                            <a href="shop.html" class="btn btn-primary btn-link btn-underline">Shop
                                Electronics<i class="d-icon-arrow-right"></i></a>
                            <figure class="floating y-50" data-options='{"invertX":false,"invertY":false}'
                                data-floating-depth=".4">
                                <img class="layer h-auto" src="{{ asset('ui/frontend/images/demos/demo6/slides/1-floating.png') }}"
                                    alt="power bank" width="778" height="394" />
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            <section class="product-wrapper mb-10 pb-8 appear-animate">
                <div class="container">
                    <div class="toolbox">
                        <div class="toolbox-left">
                            <ul class="nav-filters product-filters" data-target="#products-grid">
                                <li><a href="#" class="nav-filter active font-weight-semi-bold"
                                        data-filter="*">All</a></li>
                                <li><a href="#" class="nav-filter font-weight-semi-bold"
                                        data-filter=".accessories">Accessories</a></li>
                                <li><a href="#" class="nav-filter font-weight-semi-bold"
                                        data-filter=".bags">Bags</a></li>
                                <li><a href="#" class="nav-filter font-weight-semi-bold"
                                        data-filter=".electronics">Electronics</a></li>
                                <li><a href="#" class="nav-filter font-weight-semi-bold"
                                        data-filter=".essentials">Essentials</a></li>
                                <li><a href="#" class="nav-filter font-weight-semi-bold"
                                        data-filter=".sale">Sale</a></li>
                            </ul>
                            <span class="divider"></span>
                            <div class="header-search hs-toggle">
                                <a href="#" class="search-toggle">
                                    <i class="d-icon-search"></i>Search
                                </a>
                                <form action="#" class="input-wrapper">
                                    <input type="text" class="form-control" name="search" autocomplete="off"
                                        placeholder="Search your keyword..." required />
                                    <button class="btn btn-search" type="submit" title="submit-button">
                                        <i class="d-icon-search"></i>
                                    </button>
                                </form>
                            </div>
                            <!-- End of Header Search -->
                        </div>
                        <div class="toolbox-right">
                            <a href="#" class="btn btn-link  right-sidebar-toggle font-weight-semi-bold mr-0"><i
                                    class="d-icon-filter-3"></i>Filter</a>
                        </div>
                    </div>
                    <div class="row grid products-grid mb-2" id="products-grid" data-grid-options="{
                        'masonry': {
                            'columnWidth': ''
                        }
                    }">
              
                    <div class="row">
                        <x-frontend.layouts.elements.product/>
                        <x-frontend.layouts.elements.product/>
                        <x-frontend.layouts.elements.product/>

                    </div>
                    <!-- /row -->
                </div>
                <!-- /container -->
 

</x-frontend.layouts.master>
        {{-- </x-components.frontend.layouts.master> --}}