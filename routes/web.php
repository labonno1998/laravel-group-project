<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('frontend.home');
})->name('frontend.home');

Route::get('/product', function () {
    return view('frontend.product');
})->name('frontend.product');

Route::get('/order', function () {
    return view('frontend.order');
})->name('frontend.order');

Route::get('/order', function () {
    return view('frontend.order-complete');
})->name('frontend.order-complete');

Route::get('/cart', function () {
    return view('frontend.cart');
})->name('frontend.cart');

Route::get('/store', function () {
    return view('frontend.store');
})->name('frontend.store');

Route::get('/checkout', function () {
    return view('frontend.checkout');
})->name('frontend.checkout');

Route::get('/contact', function () {
    return view('frontend.contact');
})->name('frontend.contact');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';
